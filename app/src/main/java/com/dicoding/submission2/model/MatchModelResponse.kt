package com.dicoding.submission2.model

data class MatchModelResponse(
    val events: List<MatchModel>? = null
)